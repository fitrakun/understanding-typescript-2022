// testing class transformer 
import "reflect-metadata";
import { plainToClass } from "class-transformer";
import { validate } from 'class-validator';

import { Product } from './product.model';

const products = [
    { title: 'Book A', price: 29.99},
    { title: 'A Carpet', price: 19.99 }
]

const loadedProduct = plainToClass(Product, products);

const newProduct = new Product('',-5.99);
validate(newProduct).then(error=>{
    if(error.length > 0){
        console.log("Validation errors");
        console.log(error);
    } else {
        console.log(newProduct.getInformation());
    }
});

for (const prod of loadedProduct){
    console.log(prod.getInformation());
}


// testing lodash
// import _ from 'lodash';

// console.log(_.shuffle([1,2,3]));

// import { ProjectInput } from "./components/project-input";
// import { ProjectList } from "./components/project-list";

// new ProjectInput();
// new ProjectList('active');
// new ProjectList('finished');